package com.gdstruc.module2;

import java.util.Objects;

public class PlayerNode
{
    private Player player; //int data
    private PlayerNode nextPlayer; //ref to next node
    private PlayerNode prevPlayer; //ref to prev node

    public PlayerNode(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayerNode getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(PlayerNode nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public PlayerNode getPrevPlayer() {
        return prevPlayer;
    }

    public void setPrevPlayer(PlayerNode prevPlayer) {
        this.prevPlayer = prevPlayer;
    }

    @Override
    public String toString() {
        return "PlayerNode{" +
                "player=" + player +
                '}';
    }


}
