package com.gdstruc.module2;

public class Main {

    public static void main(String[] args) {

        PlayerLinkedList list = new PlayerLinkedList();
        Player merlin = new Player(1, "Merlin", 999);
        Player arthur = new Player(2, "Arthur", 900);
        Player percival = new Player(3, "Percival", 120);
        Player morgana = new Player(4, "Morgana", 998);
        Player freya = new Player(5, "Freya", 9001);
        Player helya = new Player(6, "Helya", 9999);



        list.addToFront(merlin);
        list.addToFront(arthur);
        list.addToFront(percival);
        list.addToFront(morgana);
        list.addToFront(freya);
        list.addToFront(helya);


        //list.delete();  //--> Delete Function remove "//" whenever.
        list.indexOf(merlin); // returns -1 if does not exist in the list
        list.contains(merlin); //returns -1 if does not exist in the list

        list.printList();
        list.counter();







    }

}
