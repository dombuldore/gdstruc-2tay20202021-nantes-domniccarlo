package com.gdstruc.module2;

public class PlayerLinkedList
{
    private PlayerNode head;


    public void addToFront(Player player)
    {
        PlayerNode node = new PlayerNode(player);
        node.setNextPlayer(null);
        node.setNextPlayer(head);
        head = node;
    }


    public void counter() // Modified the Printlist function lmao.
    {
        PlayerNode current = head;
        int counter = 0;
        while(current != null)
        {
            current = current.getNextPlayer();
            counter++;
        }
        System.out.println("Nodes: " + counter);

    }

    public int indexOf(Player player) // Source: https://www.youtube.com/watch?v=V6qkNhvRub4&ab_channel=BetterCoder
    {
        PlayerNode current = head;
        int index = 1;
        while(current != null)
        {
            current = current.getNextPlayer();
            if(current.getPlayer().equals(player))
            {
                System.out.println("Index of " + player + " is : " + index);
                return index;
            }
            index++;
        }
        return -1;
    }

    public boolean contains(Player player) // Source : https://www.youtube.com/watch?v=V6qkNhvRub4&ab_channel=BetterCoder
    {
        return this.indexOf(player) >= 0;
    }

    public void delete()
    {
        PlayerNode current = head;
        head = current.getNextPlayer();
        System.out.println("Deleted: " + current.getPlayer());
        current.setPrevPlayer(null);
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.println(current.getPlayer() + "-> ");
            current = current.getNextPlayer();
        }
        System.out.print("<- NULL");
        System.out.println();
    }

}
