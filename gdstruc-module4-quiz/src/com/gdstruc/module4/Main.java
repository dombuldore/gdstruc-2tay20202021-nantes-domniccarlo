package com.gdstruc.module4;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        ArrayQueue matchQueue = new ArrayQueue(7);
        System.out.println("-= PLAYERBASE =-");

        System.out.println("Dombuldore");
        System.out.println("Dairu");
        System.out.println("Diviny");
        System.out.println("AegonPrime");
        System.out.println("NicolaiAlto");
        System.out.println("Chico");
        System.out.println("Cabs");

        System.out.println();

        int matches = 1;
        while(matches != 10)
        {
            //New Match
            System.out.println("--= MATCH " + matches + " =--");
            Random dice = new Random();
            System.out.println();

            //Playerbase
            System.out.println("-== PLAYERS ==-");
            matchQueue.createPlayerBase(matchQueue);
            matchQueue.printQueue();

            //Queue Count
            int playerCount = 1 + dice.nextInt(7);
            System.out.println("PLAYER(S) being queued: " + playerCount);
            System.out.println();

            if(playerCount < 5)
            {
                System.out.println("QUEUED PLAYERS:");
                matchQueue.printQueuedPlayers(playerCount);

                System.out.println();
                System.out.println("Not Enough Players. Re-queueing...");

                matchQueue.deleteAll(matchQueue); // Re-initializing everything in the process.
                System.out.println();
                System.out.println();
            }

            //5++
            else
            {
                System.out.println("QUEUED PLAYERS:");
                matchQueue.printQueuedPlayers(playerCount);
                System.out.println();
                System.out.println("Proceeding to match...");
                System.out.println();

                //popping the first 5
                for(int i = 0; i <= 5; i++)
                {
                    System.out.println("Removing : " + matchQueue.remove());
                }

                matchQueue.deleteAll(matchQueue); //CheAtCoDEs - Restarting everything.
                matches++;
                System.out.println();
                System.out.println();
                System.out.println();


            }

            //Check Match
            if(matches == 10)
            {
                break;
            }
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("MATCHMAKING SYSTEM IS OVER. " + matches + " matches has been made.");
    }
}
