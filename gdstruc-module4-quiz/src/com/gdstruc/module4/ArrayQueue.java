package com.gdstruc.module4;

import java.util.NoSuchElementException;
import java.util.Random;

public class ArrayQueue {
    private Player[] queue;
    private int front;
    private int back;

    public ArrayQueue(int capacity)
    {
        queue = new Player[capacity];
    }
    //New element is added at the BACK
    //Back = Length - 1

    public void add(Player player)
    {
        //resizes the array if its full
        if(back == queue.length)
        {
            Player[] newArray = new Player[queue.length * 2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;
        }

        queue[back] = player;
        back++;
    }

    public Player remove()
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }
        Player removedPlayer = queue[front];
        queue[front] = null;
        front++;

        if (size() == 0) // reset trackers when queue is empty
        {
            front = 0;
            back = 0;
        }

        return removedPlayer;
    }

    public Player peek()
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public void printQueue()
    {
        for (int i = front; i < back; i++)
        {
            System.out.println(queue[i]);
        }
    }

    public void createPlayerBase(ArrayQueue matchQueue)
    {
        matchQueue.add(new Player(1, "Dombuldore", 1650));
        matchQueue.add(new Player(2, "Dairu", 1325));
        matchQueue.add(new Player(3, "Diviny", 1890));
        matchQueue.add(new Player(4, "AegonPrime", 765));
        matchQueue.add(new Player(5, "BattleBunny", 1799));
        matchQueue.add(new Player(6, "Chico", 1899));
        matchQueue.add(new Player(7, "CabbaKappa", 1999));
    }

    public void createMatchmaking(ArrayQueue matchQueue, int size)
    {
        for(int i = front; i <= size - 1; i++)
        {
            matchQueue.remove();
        }
    }

    public void printQueuedPlayers(int number)
    {
        for(int i = front; i <= number - 1; i++)
        {
            System.out.println(queue[i]);
        }
    }

    public void deleteAll(ArrayQueue matchQueue)
    {
        for(int i = front; i < back; i++)
        {
            matchQueue.remove();
        }
    }





    public int size()
    {
        return back - front;
    }

}
