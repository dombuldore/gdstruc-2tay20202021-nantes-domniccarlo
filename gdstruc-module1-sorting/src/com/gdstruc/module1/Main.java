package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] numbers = new int[10];

        numbers[0] = 35;
        numbers[1] = 69;
        numbers[2] = 1;
        numbers[3] = 10;
        numbers[4] = -50;
        numbers[5] = 320;
        numbers[6] = 63;
        numbers[7] = 58;
        numbers[8] = 26;
        numbers[9] = 13;

        System.out.println("Before Sorting");
        printArray(numbers);

        System.out.println("\n\n ---------------- ");

        bubbleSortAscend(numbers);
        System.out.println("\n\nAfter Bubble Sort (Ascending)");
        printArray(numbers);

        bubbleSortDescend(numbers);
        System.out.println("\n\nAfter Bubble Sort (Descending)");
        printArray(numbers);

        System.out.println("\n\n ---------------- ");

        selectionSortAscend(numbers);
        System.out.println("\n\nAfter Selection Sort (Ascending)");
        printArray(numbers);

        selectionSortDescend(numbers);
        System.out.println("\n\nAfter Selection Sort (Descending)");
        printArray(numbers);


    }

    //QUIZ 1 - Modify the Sorting Algorithms so that they are in Descending Order
    //PERSONAL NOTES-------------------------------------------------
    private static void bubbleSortAscend(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if(arr[i] > arr[i+1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
    }
    private static void bubbleSortDescend(int[] arr)
    {
        for (int lastSortedIndex = 0; lastSortedIndex < arr.length - 1; lastSortedIndex++)
        //Sorting from el.0 -> arr.length - 1; changed to "<"
        {
            for (int i = 0; i < arr.length - 1; i++) // traversal
                //lastSortedIndex becomes arr.length-1
            {
                if (arr[i] < arr[i+1]) //comparison if element is greater than the other element
                    // changed to "<" instead of ">"
                {
                    int temp = arr[i]; //store the value first
                    arr[i] = arr[i+1]; // becomes the value of the next one
                    arr[i+1] = temp; //becomes swapped
                }


            }
        }
    }

    //Modify the Selection Srt to look for the SMALLEST VALUE FIRST and PUT IT AT THE END INSTEAD OF LOOKING FOR THE
    //LARGEST INSTEAD OF PUTTING IT AT THE BEGINNING.
    private static void selectionSortAscend(int[] arr)
    {
        for(int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int largestIndex = 0;

            for (int i = 0; i < lastSortedIndex; i++)
            {
                if(arr[i] > arr[largestIndex])
                {
                    largestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[largestIndex];
            arr[largestIndex] = temp;
        }
    }

    private static void selectionSortDescend(int[] arr)
    {
        for(int lastSortedIndex = 0; lastSortedIndex < arr.length - 1; lastSortedIndex++)
        //Sorting from el.0 -> arr.length - 1; changed to "<"
        {
            int smallestIndex = lastSortedIndex; //changed to lastSortedIndex

            for (int i = lastSortedIndex + 1; i < arr.length; i++)
            {
                if(arr[i] >= arr[smallestIndex])
                //element i is compared to lastSortedIndex
                    // if element i is less than lastSortedIndex then it becomes the smallestIndex
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[smallestIndex];
            arr[smallestIndex] = arr[lastSortedIndex];
            arr[lastSortedIndex] = temp;
        }
    }

    private static void printArray(int[] arr)
    {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }


}
