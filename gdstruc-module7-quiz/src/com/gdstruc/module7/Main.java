package com.gdstruc.module7;

public class Main {

    public static void main(String[] args) {

        Tree tree = new Tree();

        tree.insert(25);
        tree.insert(17);
        tree.insert(29);
        tree.insert(10);
        tree.insert(16);
        tree.insert(-5);
        tree.insert(60);
        tree.insert(55);

        //Ascending Order
        System.out.println("Data from Min->Max");
        tree.traverseInOrder();
        System.out.println();

        tree.getMax(); // Source: https://www.javatpoint.com/java-program-to-find-the-largest-element-in-a-binary-tree
        tree.getMin(); // Edited it to work with the whole code.
        System.out.println();

        //Descending Order
        System.out.println("Data from Max->Min");
        tree.reverseInOrder();
        tree.getMax();
        tree.getMin();

    }
}
