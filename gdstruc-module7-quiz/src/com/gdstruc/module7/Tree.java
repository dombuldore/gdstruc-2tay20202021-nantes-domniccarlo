package com.gdstruc.module7;

public class Tree {

    private Node root;

    public void insert(int value)
    {
        if (root == null)
        {
            root = new Node(value);
        }
        else
        {
            root.insert(value);
        }
    }

    public void traverseInOrder()
    {
        if (root != null)
        {
            root.traverseInOrder();
        }
    }

    public void reverseInOrder()
    {
        if(root != null)
        {
            root.reverseInOrder();
        }
    }

    public void getMax()
    {
        System.out.println("Max Data: " + root.getMax(root));
    }

    public void getMin()
    {
        System.out.println("Min Data: " + root.getMin(root));
    }

    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

}
