package com.gdstruc.module3;
import java.util.Random;


public class Main {

    public static void main(String[] args) {
        //Card Names (30)
        //Tranquility, War, Neutrality, Enchantment, Sin
        CardStack mainDeck = new CardStack();
        CardStack hand = new CardStack();
        CardStack discardPile = new CardStack();

        //Flavor Text
        System.out.println();
        System.out.println("-== COMMANDS ==-");
        System.out.println("Command 1: Draw X Cards from MAIN DECK -> HAND");
        System.out.println("Command 2: Discard X Cards from HAND -> DISCARD PILE");
        System.out.println("Command 3: Get X Cards from DISCARD PILE -> HAND");
        System.out.println("NOTE: If Dice Roll exceeded remaining deck size:\n" + " remaining cards will be drawn discarded regardless of Dice Roll.");
        System.out.println();

        //Initialization
        mainDeck.createDeck();

        System.out.println();
        System.out.println("HAND PILE");
        hand.printCardStack();

        System.out.println();
        System.out.println("DISCARD PILE");
        discardPile.printCardStack();

        System.out.println();
        System.out.println("++ STATS ++");
        System.out.println("MAIN DECK: " + mainDeck.size());
        System.out.println("HAND: " + hand.size());
        System.out.println("DISCARD PILE: " + discardPile.size());
        int round = 1;
        System.out.println();
        System.out.println();

        mainDeck.pressAnyKeyToContinue();
        System.out.flush();


        while(! mainDeck.isEmpty())
        {
            //Round Count
            System.out.println("--== ROUND : " + round + " ==--");


            //Roll For Command
            Random commandDice = new Random();
            Random diceRoll = new Random();
            int result = 1 + commandDice.nextInt(3);
            int diceRollResult = 1 + diceRoll.nextInt(5);

            //Flavor Text
            System.out.println("COMMAND_RESULT: " + result);
            System.out.println("ROLL_RESULT: " + diceRollResult);
            System.out.println();

            if (result == 1) // Draw X Cards [MAIN DECK -> HAND]
            {
                if(diceRollResult >= mainDeck.size())
                {
                    System.out.println("Random Number generated exceeded Deck Size.");
                    System.out.println("Drawing " + mainDeck.size() + " Cards from Main Deck Pile to Hand Pile.");
                    //--------- CHEAT CODES ---------//
                    mainDeck.drawCard(hand, mainDeck.size());

                    //Printing the other Decks
                    System.out.println();
                    System.out.println("MAIN DECK PILE");
                    mainDeck.printCardStack();

                    System.out.println();
                    System.out.println("DISCARD PILE");
                    discardPile.printCardStack();

                    System.out.println();
                    System.out.println("++ STATS ++");
                    System.out.println("MAIN DECK: " + mainDeck.size());
                    System.out.println("HAND: " + hand.size());
                    System.out.println("DISCARD PILE: " + discardPile.size());
                }
                else
                {
                    System.out.println("You rolled for: " + diceRollResult);
                    System.out.println("Drawing " + diceRollResult + " cards from MAIN DECK.");
                    System.out.println();
                    mainDeck.drawCard(hand, diceRollResult);

                    //Printing the other Decks
                    System.out.println();
                    System.out.println("MAIN DECK PILE");
                    mainDeck.printCardStack();

                    System.out.println();
                    System.out.println("DISCARD PILE");
                    discardPile.printCardStack();

                    //Stats and shit
                    System.out.println();
                    System.out.println("++ STATS ++");
                    System.out.println("MAIN DECK: " + mainDeck.size());
                    System.out.println("HAND: " + hand.size());
                    System.out.println("DISCARD PILE: " + discardPile.size());
                    System.out.println();
                }
            }

            else if (result == 2) // Discard X Cards [HAND -> DISCARD PILE]
            {
                if(hand.isEmpty())
                {
                    System.out.println("...Empty Deck...");
                    System.out.println("...Rolling again...");
                    System.out.println();
                    continue;
                }
                if(diceRollResult >= hand.size())
                {
                    System.out.println("Random Number generated exceeded or equal to Deck Size.");
                    System.out.println("Discarding " + hand.size() + " Remaining Cards to Discard Pile.");
                    //--------- CHEAT CODES ---------//
                    hand.discardCard(discardPile, hand.size());

                    //Printing the other Decks
                    System.out.println();
                    System.out.println("MAIN DECK PILE");
                    mainDeck.printCardStack();

                    System.out.println();
                    System.out.println("HAND PILE");
                    hand.printCardStack();

                    //Stats and shit
                    System.out.println();
                    System.out.println("++ STATS ++");
                    System.out.println("MAIN DECK: " + mainDeck.size());
                    System.out.println("HAND: " + hand.size());
                    System.out.println("DISCARD PILE: " + discardPile.size());
                    System.out.println();
                }
                else
                {
                    System.out.println("You rolled for: " + diceRollResult);
                    System.out.println("Discarding " + diceRollResult + " cards from your HAND.");
                    hand.discardCard(discardPile, diceRollResult);

                    //Printing the other Decks
                    System.out.println();
                    System.out.println("MAIN DECK PILE");
                    mainDeck.printCardStack();

                    System.out.println();
                    System.out.println("HAND PILE");
                    hand.printCardStack();

                    //Stats and shit
                    System.out.println();
                    System.out.println("++ STATS ++");
                    System.out.println("MAIN DECK: " + mainDeck.size());
                    System.out.println("HAND: " + hand.size());
                    System.out.println("DISCARD PILE: " + discardPile.size());
                    System.out.println();
                }


            }

            else // Get X Cards from Discard Pile [DISCARD PILE -> HAND]
            {
                if(discardPile.isEmpty() || hand.isEmpty())
                {
                    System.out.println("...Empty Deck...");
                    System.out.println("...Rolling again...");
                    System.out.println();
                    continue;
                }
                if(diceRollResult >= discardPile.size()) //If Roll Exceed number of remaining discarded cards; will get the rest regardless of number rolled.
                {
                    System.out.println("Random Number generated exceeded Deck Size.");
                    System.out.println("Drawing " + discardPile.size() + " Remaining Cards from Discard Pile.");
                    discardPile.getCard(hand, discardPile.size());
                }
                else
                {
                    System.out.println("You rolled for: " + diceRollResult);
                    System.out.println("Getting " + diceRollResult + " cards from DISCARD PILE.");
                    discardPile.getCard(hand, diceRollResult);

                    //Printing the other Decks
                    System.out.println();
                    System.out.println("MAIN DECK PILE");
                    mainDeck.printCardStack();

                    System.out.println();
                    System.out.println("DISCARD PILE");
                    discardPile.printCardStack();

                    //Stats
                    System.out.println();
                    System.out.println("++ STATS ++");
                    System.out.println("MAIN DECK: " + mainDeck.size());
                    System.out.println("HAND: " + hand.size());
                    System.out.println("DISCARD PILE: " + discardPile.size());
                }

            }
            if(mainDeck.isEmpty())
            {
                System.out.println();
                System.out.println();
                System.out.println(".GAME OVER.");
                System.out.println("Game Ended after: " + round + " Rounds! Thank you.");

                System.out.println();
                System.out.println("++ GAME OVER STATS ++");
                System.out.println("MAIN DECK: " + mainDeck.size());
                System.out.println("HAND: " + hand.size());
                System.out.println("DISCARD PILE: " + discardPile.size());

                break;
            }

            else
            {
                round++;
                System.out.println();
            }

        }












    }
}
