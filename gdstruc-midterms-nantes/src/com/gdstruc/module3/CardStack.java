package com.gdstruc.module3;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;

public class CardStack {
    private LinkedList<Card> stack;

    public CardStack()
    {
        stack = new LinkedList<Card>();
    }

    public void push(Card card)
    {
        stack.push(card);
    }

    public int size()
    {
        return stack.size();
    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public Card pop()
    {
        return stack.pop();
    }

    public Card peek()
    {
        return stack.peek();
    }

    public void printCardStack()
    {
        ListIterator<Card> iterator = stack.listIterator();
        System.out.println("-= CARD STACK =-");
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    public void cardCounter() //Keeping tabs on the number of Cards in the Players Hand
    {
        System.out.println("Card Counter: " + stack.size());
    }

    public void drawCard(CardStack hand, int x) //Command 1 - Draw X card from DECK->HAND
    {
       for(int i = 0; i <= x - 1; i++)
       {
           Card mainDeck = this.stack.pop();
           hand.push(mainDeck);

       }
        System.out.println("HAND PILE");
        hand.printCardStack();
    }

    public void discardCard(CardStack discardPile, int x) //Command 2 - Discard X card from HAND->DISCARD
    {
        for(int i = 0; i <= x - 1; i++)
        {
            Card hand = this.stack.pop();
            discardPile.push(hand);
        }
        System.out.println("DISCARD PILE");
        discardPile.printCardStack();
    }

    public void getCard(CardStack hand, int x) //Command 3 - Get x card from DISCARD->HAND
    {
        for(int i = 0; i <= x - 1; i++)
        {
            Card discardPile = this.stack.pop();
            hand.push(discardPile);
        }
        System.out.println("HAND PILE");
        hand.printCardStack();
    }

    public void createDeck() // Creates the deck and prints the stack
    {
        stack.push(new Card("One of Tranquility", 1));
        stack.push(new Card("Two of Tranquility", 2));
        stack.push(new Card("Three of Tranquility", 3));
        stack.push(new Card("Four of Tranquility", 4));
        stack.push(new Card("Five of Tranquility", 5));
        stack.push(new Card("Six of Tranquility", 6));
        stack.push(new Card("One of War", 1));
        stack.push(new Card("Two of War", 2));
        stack.push(new Card("Three of War", 3));
        stack.push(new Card("Four of War", 4));
        stack.push(new Card("Five of War", 5));
        stack.push(new Card("Six of War", 6));
        stack.push(new Card("One of Neutrality", 1));
        stack.push(new Card("Two of Neutrality", 2));
        stack.push(new Card("Three of Neutrality", 3));
        stack.push(new Card("Four of Neutrality", 4));
        stack.push(new Card("Five of Neutrality", 5));
        stack.push(new Card("Six of Neutrality", 6));
        stack.push(new Card("One of Enchantment", 1));
        stack.push(new Card("Two of Enchantment", 2));
        stack.push(new Card("Three of Enchantment", 3));
        stack.push(new Card("Four of Enchantment", 4));
        stack.push(new Card("Five of Enchantment", 5));
        stack.push(new Card("Six of Enchantment", 6));
        stack.push(new Card("One of Sin", 1));
        stack.push(new Card("Two of Sin", 2));
        stack.push(new Card("Three of Sin", 3));
        stack.push(new Card("Four of Sin", 4));
        stack.push(new Card("Five of Sin", 5));
        stack.push(new Card("Six of Sin", 6));

        printCardStack();
    }

    public void pressAnyKeyToContinue()
    {
        System.out.println("Press Enter Key To Continue...");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }


    @Override
    public String toString() {
        return "CardStack{" +
                "stack=" + stack +
                '}';
    }
}
