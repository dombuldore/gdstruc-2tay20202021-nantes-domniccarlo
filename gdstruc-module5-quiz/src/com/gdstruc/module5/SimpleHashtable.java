package com.gdstruc.module5;


public class SimpleHashtable {

    private StoredPlayer[] hashtable;

    public SimpleHashtable()
    {
        hashtable = new StoredPlayer[10];
    }

    //Hashing Function
    private int hashKey(String key)
    {
        return key.length() % hashtable.length;
    }

    //Add
    public void put(String key, Player value)
    {
        int hashedKey = hashKey(key);
        //Linear Probing
        if (isOccupied(hashedKey))
        {
            int stoppingIndex = hashedKey;

            //Do Linear Probing
            if(hashedKey == hashtable.length - 1)
            {
                hashedKey = 0;
            }
            else
            {
                hashedKey++;
            }

            while(isOccupied(hashedKey) && hashedKey != stoppingIndex)
            {
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        //Check if element has the same char length.
        if (isOccupied(hashedKey))
        {
            System.out.println("Sorry, there is already an element at position: " + hashedKey);
        }
        else
        {
            hashtable[hashedKey] = new StoredPlayer(key, value);
        }
    }

    //Remove
    public void remove(String key)
    {
        //Does the findKey function which has linear probing first.
        System.out.println("REMOVING : " + key);
        int hashedKey = findKey(key);
        if (hashedKey == -1)
        {
            System.out.println("ERROR: Key not found.");
            System.out.println();
        }

        //Flavor Text(s)
        System.out.println("VALUE: " + get(key));
        System.out.println();

        //make the hashedKey null gg ez.
        hashtable[hashedKey] = null;
    }

    //Get
    public Player get(String key)
    {
        int hashedKey = findKey(key);

        if (hashedKey == -1)
        {
            return null;
        }

        return hashtable[hashedKey].value;
    }

    private int findKey(String key)
    {
        int hashedKey = hashKey(key);

        //Found the key
        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }


        int stoppingIndex = hashedKey;

        //Do Linear Probing
        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }

        while (hashedKey != stoppingIndex && hashtable[hashedKey] != null &&
                !hashtable[hashedKey].key.equals(key))
        {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }

        if (hashtable[hashedKey] != null
                && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }

        return -1;


    }

    private boolean isOccupied(int index)
    {
        return hashtable[index] != null;
    }

    //Print
    public void printHashtable()
    {
        for (int i = 0; i < hashtable.length; i++)
        {
            System.out.println("Element " + i + " " + hashtable[i]);
        }
    }


}
