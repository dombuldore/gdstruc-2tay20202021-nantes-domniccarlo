package com.gdstruc.module5;

public class Main {

    public static void main(String[] args) {

        Player dombuldore = new Player(515, "Dombuldore", 99999);
        Player dairu = new Player(2, "Dairu", 1290);
        Player diviny = new Player(3, "Diviny", 1592);
        Player aegon = new Player(4, "AegonPrime", 1252);
        Player nicolai = new Player(5, "NicolaiAlto", 1789);
        Player chico = new Player(6, "Chico", 1999);

        SimpleHashtable hashtable = new SimpleHashtable();

        hashtable.put(dombuldore.getUserName(), dombuldore);
        hashtable.put(dairu.getUserName(), dairu);
        hashtable.put(diviny.getUserName(), diviny);
        hashtable.put(aegon.getUserName(), aegon);
        hashtable.put(nicolai.getUserName(), nicolai);
        hashtable.put(chico.getUserName(), chico);

        hashtable.printHashtable();
        System.out.println();
        System.out.println();

        //Removing a true value
        hashtable.remove("Dombuldore");
        hashtable.printHashtable();

        //Removing a false value
        //hashtable.remove("byefelicia");



    }
}
